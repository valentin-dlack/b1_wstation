# Partage de fichiers avec une VM 

### Pour créer un serveur sur notre PC avec samba, on doit activer la fonctionnalité :

![](https://i.imgur.com/eGtqC4S.png)

Le support de partage de fichiers SMD 1.0/CIFS est la fonctionnalité qu'on doit activer.
Après l'avoir activer, il faut redémmarer la machine

### Ensuite, on crée un dossier pour partager les fichiers, je l'ai fait sur mon bureau et l'ai appelé "share".
Il faut ensuite activer le partage de ce fichier avec cette commande :

`New-SmbShare -Name "share" -Path "C:\Users\lolva\Desktop\share"`

### Accès au partage de fichier depuis la VM :

![](https://i.imgur.com/Znmd9hT.png)

On peut voir que les fichiers sont bien partager après avoir monter le partage avec :

`mount -t cifs -o username=lolva,password=12345 //192.168.120.1/share /opt/partage`

On test enfin si les fichiers sont correctements partagés. On voit dans l'image ci-dessous que c'est le cas !

![](https://i.imgur.com/Dk3tsnc.png)

Le partage fonctionne bien.
 