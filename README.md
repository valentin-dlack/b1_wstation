# Compte Rendu TP1

## Host OS

* Nom de la machine
```ps
PS > [Environment]::MachineName
DESKTOP-098BAAD
```
* OS et Version
```ps
PS >[Environment]::OSVersion

Platform ServicePack Version      VersionString
-------- ----------- -------      -------------
 Win32NT             10.0.18363.0 Microsoft Windows NT 10.0.18363.0
```
* Architechture du processeur
```
PS > Get-CimInstance -ClassName Win32_Processor
DeviceID Name                                      Caption                               MaxClockSpeed SocketDesignation Manufacturer
-------- ----                                      -------                               ------------- ----------------- ------------
CPU0     Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz Intel64 Family 6 Model 158 Stepping 9 2808          U3E1              GenuineIntel
```
Architechture :
```
PS > [Environment]::Is64BitProcess.
true
```
* Quantité RAM / Modèle de la RAM
```
PS > Get-WmiObject win32_physicalmemory | Format-Table Manufacturer,Capacity -autosize

Manufacturer    Capacity
------------    --------
Samsung      17179869184
```
## Device
* Nombre de processeurs/coeurs
```
PS > Get-CimInstance -ClassName Win32_Processor | Format-Table NumberOfCores,NumberOfLogicalProcessors

NumberOfCores NumberOfLogicalProcessors
------------- -------------------------
            4                         8
```
* Nom du Processeur :
   * `Intel i7 - 7700HQ`
   * Intel = OEM 
   * i7 = Gamme du processeur
   * **7**000 = Génération du processeur
   * 7**700** = Gamme lié a la génération
   * HQ = Version Haute Performance pour laptop
 
 * Modèle touchpad/trackpad
```
PS > Get-CimInstance win32_POINTINGDEVICE | select hardwaretype

hardwaretype
------------
Souris HID
```
* Modèle de la carte graphique
```
PS > Get-WmiObject win32_VideoController | Format-List Name

Name : NVIDIA GeForce GTX 1050 Ti

Name : Intel(R) HD Graphics 630
```
* Marque et Modèle de mes disques durs
```
PS > wmic diskdrive get Name,Model,Size
Model                          Name                Size
HGST HTS721010A9E630           \\.\PHYSICALDRIVE1  1000202273280
Samsung SSD 860 EVO M.2 250GB  \\.\PHYSICALDRIVE0  250056737280
```
* Partitions de mes disques durs & Système de fichier
```
PS > Get-Volume

DriveLetter FriendlyName FileSystemType DriveType HealthStatus OperationalStatus SizeRemaining      Size
----------- ------------ -------------- --------- ------------ ----------------- -------------      ----
D           DATA         NTFS           Fixed     Healthy      OK                    931.38 GB 931.51 GB
E                        Unknown        CD-ROM    Healthy      Unknown                     0 B       0 B
            Récupération NTFS           Fixed     Healthy      OK                     49.46 MB    499 MB
                         FAT32          Fixed     Healthy      OK                     68.72 MB     95 MB
C                        NTFS           Fixed     Healthy      OK                    191.94 GB 232.29 GB
```
* Fonction de chaque partition
D : Data, stockage basique de données comme des documents, ect...
E : CD-ROM, disponible lorsqu'un CD est inséré
Récupération : Stocke les fichiers pour démarrer dessus ainsi que les outils de dépannage en cas de problèmes.
C : Partition de stockage

## Users

* Liste Complète des utilisateurs
```
PS > Get-WmiObject -Class Win32_UserAccount -Filter "LocalAccount=True"
AccountType : 512
Caption     : DESKTOP-098BAAD\Administrateur
Domain      : DESKTOP-098BAAD
SID         : S-1-5-21-824231070-3124326874-1089839722-500
FullName    :
Name        : Administrateur

AccountType : 512
Caption     : DESKTOP-098BAAD\DefaultAccount
Domain      : DESKTOP-098BAAD
SID         : S-1-5-21-824231070-3124326874-1089839722-503
FullName    :
Name        : DefaultAccount

AccountType : 512
Caption     : DESKTOP-098BAAD\Invité
Domain      : DESKTOP-098BAAD
SID         : S-1-5-21-824231070-3124326874-1089839722-501
FullName    :
Name        : Invité

AccountType : 512
Caption     : DESKTOP-098BAAD\lolva
Domain      : DESKTOP-098BAAD
SID         : S-1-5-21-824231070-3124326874-1089839722-1001
FullName    : valentin dautrement
Name        : lolva

AccountType : 512
Caption     : DESKTOP-098BAAD\WDAGUtilityAccount
Domain      : DESKTOP-098BAAD
SID         : S-1-5-21-824231070-3124326874-1089839722-504
FullName    :
Name        : WDAGUtilityAccount
```
* FullAdmin de la machine
  - Le compte full admin de la machine est le compte LocalSystem. Il a tout les droits sur la machine. Il fait partit du NT Authority\SYSTEM est le nom d'un SID.

## Processus

* Expliquer les processus
   * **svchost.exe** : Son rôle principal consiste à charger des bibliothèques de liens dynamiques (les .dll)
   * **lsass.exe** :  permet de vérifier les utilisateurs qui s'enregistrent à un ordinateur Windows, traite les informations essentielles sur la sécurité.
   * **fontdrvhost.exe** : il permet de gérer l'activité des polices sur le système
   * **SgrmBroker.exe** : C'est un service Windows qui fonctionne et fait partie de Windows Defender
   * **MsMpEng.exe** : C'est un processus lié a Windows Defender qui permet de scanner les fichiers pour detecter les SpyWare

* Processus Full Admin
  * Les processus éxecutés par `AUTORITE NT/System` ou `AUTORITE NT\SERVICE LOCAL | RESEAU`

## Network

* Cartes réseaux
```
PS > Get-NetAdapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Ethernet                  Realtek PCIe GbE Family Controller           16 Disconnected B0-6E-BF-23-1E-8E          0 bps
Wi-Fi                     Intel(R) Dual Band Wireless-AC 7265           8 Up           44-03-2C-BC-78-2F       400 Mbps
Connexion réseau Bluet... Bluetooth Device (Personal Area Netw...       7 Disconnected 44-03-2C-BC-78-33         3 Mbps
```
* Ports TCP/UDP 
```
PS > netstat -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:445            DESKTOP-098BAAD:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:5040           DESKTOP-098BAAD:0      LISTENING       6952
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:49664          DESKTOP-098BAAD:0      LISTENING       868
 [lsass.exe]
  TCP    0.0.0.0:49665          DESKTOP-098BAAD:0      LISTENING       780
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          DESKTOP-098BAAD:0      LISTENING       1728
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          DESKTOP-098BAAD:0      LISTENING       2376
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          DESKTOP-098BAAD:0      LISTENING       3584
 [spoolsv.exe]
  TCP    10.33.0.237:52401      a92-122-253-130:https  CLOSE_WAIT      7512
 [WinStore.App.exe]
  TCP    10.33.0.237:52567      a104-79-88-103:https   CLOSE_WAIT      2200
 [Video.UI.exe]
  TCP    10.33.0.237:52797      47:https               ESTABLISHED     3252
 [Discord.exe]
  TCP    10.33.0.237:52851      40.67.251.132:https    ESTABLISHED     11556
 [OneDrive.exe]
  TCP    10.33.0.237:53005      40.67.251.132:https    ESTABLISHED     4004
  WpnService
 [svchost.exe]
  TCP    10.33.0.237:53475      151.101.2.49:https     ESTABLISHED     7636
 [brave.exe]
  TCP    10.33.0.237:53593      25:https               TIME_WAIT       0
  TCP    10.33.0.237:53599      ec2-18-179-225-220:https  ESTABLISHED     7636
 [brave.exe]
  TCP    10.33.0.237:53600      162.159.134.232:https  ESTABLISHED     3252
 [Discord.exe]
  TCP    10.33.0.237:53601      server-99-86-95-32:https  ESTABLISHED     3252
 [Discord.exe]
  TCP    10.33.0.237:53602      ec2-54-74-191-31:https  TIME_WAIT       0
  TCP    10.33.0.237:53605      51.138.106.75:https    ESTABLISHED     5240
  CDPUserSvc_2d33109
 [brave.exe]
  TCP    127.0.0.1:6463         DESKTOP-098BAAD:0      LISTENING       1956
 [Discord.exe]
  TCP    127.0.0.1:11456        DESKTOP-098BAAD:0      LISTENING       13224
 [DashlanePlugin.exe]
  TCP    127.0.0.1:52256        DESKTOP-098BAAD:0      LISTENING       6628
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:52271        DESKTOP-098BAAD:0      LISTENING       3916
 [Dashlane.exe]
  TCP    127.0.0.1:52779        DESKTOP-098BAAD:65001  ESTABLISHED     4012
 [nvcontainer.exe]
  TCP    127.0.0.1:52792        DESKTOP-098BAAD:52256  ESTABLISHED     11704
 [NVIDIA Share.exe]
  TCP    127.0.0.1:65001        DESKTOP-098BAAD:0      LISTENING       4012
 [nvcontainer.exe]
  TCP    127.0.0.1:65001        DESKTOP-098BAAD:52779  ESTABLISHED     4012
 [nvcontainer.exe]
  TCP    [::]:49664             DESKTOP-098BAAD:0      LISTENING       868
 [lsass.exe]
  TCP    [::]:49666             DESKTOP-098BAAD:0      LISTENING       1728
  EventLog
 [svchost.exe]
  TCP    [::]:49667             DESKTOP-098BAAD:0      LISTENING       2376
  Schedule
 [svchost.exe]
  TCP    [::]:49668             DESKTOP-098BAAD:0      LISTENING       3584
 [spoolsv.exe]
  TCP    [::1]:49835            DESKTOP-098BAAD:0      LISTENING       11548
 [jhi_service.exe]
  UDP    0.0.0.0:5050           *:*                                    6952
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*                                    2192
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:53775          *:*                                    4012
 [nvcontainer.exe]
  UDP    10.33.0.237:137        *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.0.237:138        *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.0.237:1900       *:*                                    11940
  SSDPSRV
 [svchost.exe]
  UDP    10.33.0.237:5353       *:*                                    4012
 [nvcontainer.exe]
  UDP    10.33.0.237:64253      *:*                                    11940
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:1900         *:*                                    11940
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:10030        *:*                                    6628
 [NVIDIA Web Helper.exe]
  UDP    127.0.0.1:52873        *:*                                    13700
 [nvcontainer.exe]
  UDP    127.0.0.1:60474        *:*                                    2152
  NlaSvc
 [svchost.exe]
  UDP    127.0.0.1:61965        *:*                                    4348
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:64254        *:*                                    11940
  SSDPSRV
 [svchost.exe]
  UDP    [::]:5353              *:*                                    2192
  Dnscache
 [svchost.exe]
  UDP    [::]:5355              *:*                                    2192
  Dnscache
 [svchost.exe]
  UDP    [::]:53776             *:*                                    4012
 [nvcontainer.exe]
 ```
 
 
 Les programmes concernés sont indiqués dans le retour de la commande.
 * Expliquation des programmes*
   * svchost.exe : Son rôle principal consiste à charger des bibliothèques de liens dynamiques (les .dll)
   * lsass.exe : permet de vérifier les utilisateurs qui s’enregistrent à un ordinateur Windows, traite les informations essentielles sur la sécurité.
   * spoolsv.exe : Gestion des imprimantes sur le réseau
   * WinStore.App.exe : Executable du windows store
   * Video.UI.exe : Interface qui permet de filmer notre écran ou le diffuser en live
   * Discord.exe : Logiciel Discord (discussion en ligne)
   * OneDrive.exe : Logiciel de stockage en cloud
   * brave.exe : Navigateur Internet basé sur chromium
   * nvcontainer.exe : Programme de Gestion des pilotes graphiques NVidia
   * jhi_service.exe : C'est un service de gestion pour les processeurs intel core.