#!/bin/bash
#Lack_off at 09/11/2020
#This script lock or shutdown pc

if [ $# -eq 0 ]
  then
    echo "Il manque un argument"
    exit 1
fi

if [ $1 = "shutdown" ]
then
    if [ -z "$2" ]
    then
        shutdown -h now
    else 
        sleep $2
        shutdown -h now
    fi
elif [ $1 = "lock" ]
then
    if [ -z "$2" ]
    then
        cinnamon-screensaver-command --lock
    else 
        sleep $2
        cinnamon-screensaver-command --lock
    fi
else 
    echo "Argument invalide !"
fi

