#!/bin/bash
#Lack_off at 19/10/2020
#This script displays basic infos like Machine Name, Ip, OS Name ect...
ip=`ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/'`
osver=`lsb_release -sd`
uptime=`uptime -ps`
memUsed=`free -h | grep -vE 'total|libre|d' | awk '{print $2}'`
memFree=`free -h | grep -vE 'total|libre|B' | awk '{print $4}'`
diskFree=`df -H | grep -vE '^Filesystem|tmpfs|cdrom|sr0|udev|sda1|fichier' | awk '{print $4}'`
diskUsed=`df -H | grep -vE '^Filesystem|tmpfs|cdrom|sr0|udev|sda1|fichier' | awk '{print $3}'`

echo "Checking Updates..."
testUpdt=$(apt-get --simulate upgrade | grep -vE 'Lecture|Construction|Calcul|NOTE|besoin|pour|aussi|la')
echo "Testing Download speed... (Might be long)"
download=`speedtest --simple | grep 'Mbit'`
echo "Pinging 8.8.8.8 and calculating average ping..."
pingAvg=`ping -c 4 8.8.8.8 | tail -1| awk '{print $4}' | cut -d '/' -f 2`

echo "------------------- OS -------------------"
echo "Nom de la machine : $HOSTNAME"
echo "IP Principale : $ip"
echo "Os et version de l'OS : $osver"
echo "En Activité depuis : $uptime"
echo "Paquets non mis à jours :"
echo "  $testUpdt"
echo "------------------------------------------"
echo " "
echo "------------------- RAM/Disques Physques -------------------"
echo "Utilisation de la ram :"
echo "  Free : $memFree"
echo "  Used : $memUsed"
echo "Utilisation du disque dur:"
echo "  Free : $diskFree"
echo "  Used : $diskUsed"
echo "------------------------------------------------------"
echo " "
echo "------------------- SpeedTest/Ping -------------------"
echo "Résultat du SpeedTest :"
echo "$download"
echo " "
echo "Moyenne de ping pour '8.8.8.8' :"
echo "  $pingAvg ms"
echo "------------------------------------------------------"
users=`cut -d: -f1 /etc/passwd`
echo "$users"